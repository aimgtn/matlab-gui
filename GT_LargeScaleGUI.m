function GT_LargeScaleGUI(Q, Xc, Yc,l1Neuron)
% Creates a GUI for simulating a large network of neurons. In this case half
% of the neurons in the first layer get random inputs during stimulation.
% Second layer gets inputs (excitatory/inhibitory) from the first layer.
% There is no feedback to the first layer. Rasters and PCA trajectories are
% plotted for visualization.

% Copyright (c) [2018] Washington University  in St. Louis Created by:
% [Darshit Mehta, Ahana Gangopadhyay, Kenji Aono, Shantanu Chakrabartty] 1.
% Gangopadhyay, A., and Chakrabartty, S. (2017). Spiking, bursting, and
% population dynamics in a network of growth transform neurons. 2.
% Gangopadhyay, A., Chatterjee, O., and Chakrabartty, S. (2017). Extended
% polynomial growth transforms for design and training of generalized
% support vector machines. IEEE Transactions on Neural Networks and
% Learning Systems 3.  Gangopadhyay, A., Aono, K.  Mehta, D., and
% Chakrabartty, S. (in Review). A Coupled Network of Growth Transform
% Neurons for Spike-Encoded Auditory Feature Extraction
% 
% Washington University hereby grants to you a non-transferable,
% non-exclusive, royalty-free, non-commercial, research license to use and
% copy the computer code provided here (the �Software�).  You agree to
% include this license and the above copyright notice in all copies of the
% Software.  The Software may not be distributed, shared, or transferred to
% any third party.  This license does not grant any rights or licenses to
% any other patents, copyrights, or other forms of intellectual property
% owned or controlled by Washington University.  If interested in obtaining
% a commercial license, please contact Washington University's Office of
% Technology Management (otm@dom.wustl.edu).
% 
% YOU AGREE THAT THE SOFTWARE PROVIDED HEREUNDER IS EXPERIMENTAL AND IS
% PROVIDED �AS IS�, WITHOUT ANY WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED,
% INCLUDING WITHOUT LIMITATION WARRANTIES OF MERCHANTABILITY OR FITNESS FOR
% ANY PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF ANY THIRD-PARTY PATENT,
% COPYRIGHT, OR ANY OTHER THIRD-PARTY RIGHT.  IN NO EVENT SHALL THE
% CREATORS OF THE SOFTWARE OR WASHINGTON UNIVERSITY BE LIABLE FOR ANY
% DIRECT, INDIRECT, SPECIAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN
% ANY WAY CONNECTED WITH THE SOFTWARE, THE USE OF THE SOFTWARE, OR THIS
% AGREEMENT, WHETHER IN BREACH OF CONTRACT, TORT OR OTHERWISE, EVEN IF SUCH
% PARTY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. YOU ALSO AGREE THAT
% THIS SOFTWARE WILL NOT BE USED FOR CLINICAL PURPOSES.

C =[0    0.4470    0.7410; 0.8500    0.3250    0.0980; 0.9290    0.6940    0.1250;...
    0.4940    0.1840    0.5560 ;0.4660    0.6740    0.1880];
% colors for plots
nNeuron = size(Q,1);
arrowCord = [max(Xc(1:l1Neuron)) + 0.1, min(Xc(l1Neuron+1:end))];
figNumber = figure;
set(figNumber,'NumberTitle','off',...
    'Name','Growth Transform Neuron Network Model',...
    'Units','normalized','toolbar','figure',...
    'Position',[0.05 0.1 0.9 0.8]);
h1 = axes('Position',[0.05 0.5 0.9 0.4]);
colormap(h1,'hot')
caxis([0 5])
hold on
hGraph = scatter(h1,Xc,Yc,[],zeros(nNeuron,1),'filled');
text(arrowCord(1)+0.5,0,'\rightarrow','FontSize',14,'FontWeight','Bold')
text(mean(Xc(1:l1Neuron)),max(Yc(1:l1Neuron))+2,'Level 1 neurons',...
    'HorizontalAlignment','center','FontSize',14,'FontWeight','Bold' )
text(mean(Xc(l1Neuron+1:end)),max(Yc(1:l1Neuron))+2,'Level 2 neurons',...
    'HorizontalAlignment','center','FontSize',14,'FontWeight','Bold')
xticks([]);
xticklabels([]);
yticks([]);
yticklabels([]);


colorMap = repmat(linspace(0,0.7,30)',1,3);
colorMap = [colorMap;[1 1 1];colorMap(end:-1:1,:)];
colorMap(1:30,1) = 1;
colorMap(32:61,3) = 1;
hConn=axes('Position',[0.05 0.1 0.25 0.25]);
conn_im = imagesc(hConn,Q');
colormap(hConn,colorMap)
set(gca,'xtick',[],'ytick',[])
xlabel('Post-synaptic')
ylabel('Pre-synaptic')
title('Connectivity Matrix')
caxis([-1 1])
caxis manual
colorbar

hPCA=axes('Position',[0.35 0.1 0.25 0.25]);
title('PCA trajectories')
view(3)
hRaster=axes('Position',[0.65 0.1 0.25 0.25]);
title('Raster plot')

% Pause button
runFlag = 0;
runButton = uicontrol('Style', 'pushbutton','String','Run',...
    'Min',0,'Max',1,'Value',0, 'Units','normalized', ...
    'Position',[0.8 0.02 0.1 0.05],...
    'tag','runFlag','Callback',@changepars);
%%
nIter = 10;
% rng(1234,'twister');

iInput0 = zeros(nNeuron,1)-0.01;
iInput0(l1Neuron+1:end) = -0.01;

nTimes = 30;
tStim = [10 20];
nStims = 3;
D = zeros(nNeuron,nIter*nTimes,nStims);
SPK = zeros(nNeuron,nIter*nTimes,nStims);
axes(h1)
o1 = 0;
while ishandle(figNumber)
    if runFlag
        o1 = mod(o1,3)+1;
        runbutton.Enable = 'off';
        axes(h1)
        Y = [];
        S = [];
        yInit = zeros(nNeuron,1);
        iInput = iInput0;
        inputIdx = randperm(l1Neuron,l1Neuron/2);
        iInput(inputIdx,:) = 0.05*randn(l1Neuron/2,1);
        txt1 = text(min(Xc(1:l1Neuron))-0.5,mean(Yc(1:l1Neuron)),'',...
            'HorizontalAlignment','right','FontSize',14,'FontWeight','Bold');
        for t1 = 1:nTimes
            if t1>tStim(1) && t1<tStim(2)
                I = iInput;
                txt1.String = 'Stimulus';
                txt1.Color = C(o1,:);
            else
                I = iInput0;
                txt1.String = '';
            end
            [yTarget,  yInit, spk] = GT_LargeScaleFun(Q,I,nIter+1,yInit);
            Y = cat(2,Y,yTarget(:,2:end));
            S = cat(2,S,spk);
            hGraph.CData = sum(spk,2);
            drawnow
            pause(0.1)
        end
        runFlag = 0;
        runButton.Value = 0;
        runbutton.Enable = 'on';
        D(:,:,o1) = Y;
        SPK(:,:,o1) = o1*S;
        PCA_plot(D(l1Neuron+1:end,tStim(1)*nIter:tStim(2)*nIter,:),C,3,hPCA)
        title('PCA of level 2 neurons')
        
        axes(hRaster)
        imagesc([SPK(:,:,1) SPK(:,:,2) SPK(:,:,3)])
        colormap(hRaster,[1 1 1; C(1:3,:)])
        title('Raster plot')
        
    end
    pause(0.1)
end
% Functions
    function changepars(source, ~)
        t = source.Tag;
        switch t
            case 'runFlag'
                runFlag = source.Value;
                
        end
    end
    
end
function PCA_plot(data,C,nFilt,figNumber)
nCh = size(data,1);
X = reshape(data,nCh,[]);

X = X';

k = size(data,3);
[~,score,latent] = pca(X);
D2 = round(10000*latent/sum(latent))/100;
X_PCA2 = score(:,1:3);
X_PCA2 = filtfilt(ones(1,nFilt),nFilt,X_PCA2);
data_PCA = permute(reshape(X_PCA2',3,size(data,2)/1,[]),[2 1 3]);
axes(figNumber)
cla(figNumber)
hold on
view(3)
for o1 = 1:k
    plot3(data_PCA(:,1,o1),data_PCA(:,2,o1),data_PCA(:,3,o1),...
        'Color',C(o1,:),'linewidth',1.5,'Marker','o','MarkerFaceColor',C(o1,:),'MarkerEdgeColor',C(o1,:));
end
grid on
box off
xlabel(['PC1 (' num2str(D2(1)) '%)'],'FontName','Arial','FontSize',10,'FontWeight','Bold')
ylabel(['PC2 (' num2str(D2(2)) '%)'],'FontName','Arial','FontSize',10,'FontWeight','Bold')
zlabel(['PC3 (' num2str(D2(3)) '%)'],'FontName','Arial','FontSize',10,'FontWeight','Bold')
end
            